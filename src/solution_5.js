let {notify} = require('./commons');
let {compose,run} = require('./functional');

let repository = {
  "200": {
    "offer-id": 200,
    "percentage": 15,
    "active": true,
    "requests": 0
  },
  "400": {
    "offer-id": 400,
    "percentage": 22,
    "active": false,
    "requests": 0
  }
};



function validateId(state) {
  if (typeof state.id !== 'string') {
    state.response = {
      "code": 400,
      "body": {
        "errors": ["The id you provided is invalid"]
      }
    };
  }
  return state;
}

function findOfferById(state) {
  //precondition
  if (state.response) return state;
  if (!repository[state.id])
    state.response = {
      "code": 404,
      "body": {
        "errors": ["The id you provided cannot be found"]
      }
    };
  else if (repository[state.id].active) {
    state.response = {
      "code": 200,
      "body": repository[state.id]
    }
  } else {
    notify("Offer expired: " + state.id);
    state.response = {
      "code": 400,
      "body": {
        "errors": ["The offer expired"]
      }
    };
  }
  return state;
}

function updateOffer(state) {
  //precondition
  if (state.response && state.response.code != 200) return state;
  let offer = repository[state.id];
  offer.requests++;
  state.response = {
    "code": 200,
    "body": offer
  };
  return state;
}

function json(state) {
  return JSON.stringify(state.response);
}


function getOfferById(requestId) {
  return run({
    id: requestId
  },
  [
    validateId,
    findOfferById,
    updateOffer,
    json
  ]);
}


module.exports = {
  getOfferById:getOfferById
};




