let {notify} = require('./commons');
let {compose,safe} = require('./functional');

let repository = {
  "200": {
    "offer-id": 200,
    "percentage": 15,
    "active": true,
    "requests": 0
  },
  "400": {
    "offer-id": 400,
    "percentage": 22,
    "active": false,
    "requests": 0
  }
};





function validateId(state) {
  if (typeof state.id !== 'string') throw {
    code: 400,
    body: {
      errors: ["The id you provided is invalid"]
    }
  };
  return state;
}

function findOfferById(state) {
  if (!repository[state.id]) {
    notify("Offer not found: " + state.id);
    throw {
      code: 404,
      body: {
        errors: ["The id you provided cannot be found"]
      }
    };
  }
  state.offer = repository[state.id];
  return state;
}

function validateOfferNotExpired(state) {
  if (!state.offer.active) {
    notify("Offer expired: " + state.id);
    throw {
      code: 400,
      body: {
        errors: ["The offer expired"]
      }
    };
  }
  return state;
}

function updateOffer(state) {
  state.offer.requests++;
  return state;
}

function offerToJson(state){
  return JSON.stringify({code:200,body:state.offer});
}


function getOfferById(requestId) {
  return safe({
    id: requestId
  },
  [
    validateId,
    findOfferById,
    validateOfferNotExpired,
    updateOffer,
    offerToJson
  ]);
}


module.exports = {
  getOfferById:getOfferById
};




