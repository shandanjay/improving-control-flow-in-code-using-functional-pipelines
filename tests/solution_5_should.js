let {getOfferById} = require('./../src/solution_5');
let assert = require('assert');


describe('functional pipeline (with flag) endpoint should', function(){
  it('return an offer 200', function(){
    assert.equal("{\"code\":200,\"body\":{\"offer-id\":200,\"percentage\":15,\"active\":true,\"requests\":1}}", getOfferById("200"));
  });
  it('return a validation error 400 when not found', function(){
    assert.equal("{\"code\":400,\"body\":{\"errors\":[\"The id you provided is invalid\"]}}", getOfferById(400));
  });
  it('return a validation error 400 when not active', function(){
    assert.equal("{\"code\":400,\"body\":{\"errors\":[\"The offer expired\"]}}", getOfferById("400"));
  });
  it('return a not found 404', function(){
    assert.equal("{\"code\":404,\"body\":{\"errors\":[\"The id you provided cannot be found\"]}}", getOfferById("404"));
  });
});




